# Sidebar for device tracker
# Initial: 27 November 2023
# Revision: 23_ January 2024
# Data Team: Jon and Ray

# Construct the header and sidebar parts of the UI

## Sidebar
sidebar <- dashboardSidebar(
  
  h3("Agri Northwest"),
  h4("Truck Tracker"), 
  
  sidebarMenu(
    
    h5("Farm Geography"),
    
    menuItem(h5("Fields, Offload, and Shop"),
             tabName = 'geography'),
    
    h5("Harvest Dates"),
    
    dateInput(
      "date",
      label = h5('Select Date'),
      value = '2023-10-02',
      min = '2023-10-02',
      max = '2023-10-02'
    ), 
    
    h5("Time"),
    
    menuItem('Round Trip',
             tabName = 'round_trip_time'),
    
    menuItem("Harvest Field",
             tabName = 'field_time'),
    
    menuItem('Offload Site',
             tabName = 'offload_time'),
    
    h5("Speed"),

    menuItem('All Trucks',
             tabName = 'speed_summary'),
    
    menuItem('Individual Truck',
             tabName = "individual"),
    
    h5("Real Time Monitor"),
    
    menuItem('Harvest Field',
             tabName = 'field_monitor'),
    
    menuItem('Offload Site',
             tabName = 'offload_monitor'),
    
    menuItem('Shop',
             tabName = 'shop_monitor')
    
  ))