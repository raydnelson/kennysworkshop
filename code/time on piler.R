# Time on piler function
# Initial: 29 April 2024
# Revision: 6 May 2024
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  leaflet,
  data.table,
  clock,
  arcgisbinding,
  dbscan,
  DT,
  gt,
  tidyverse
)

# load data

load("F:/Acuitus/AgriNW/loads.RData")
load("F:/Acuitus/AgriNW/shapes.RData")
load("F:/Acuitus/AgriNW/devices.RData")

# function of unload time using rleid

aa_piler_rleid <- function(truck_obs) {
  truck_obs |> 
    mutate(stop = rleid(speed_status)) |> # runs calculations on speed_status
    filter(speed_status == "Stop") |> # choose on the stop points
    mutate(stop_group = (stop %/% 2)) # choose the even stop points
}

# dbscan groups function

aa_piler_dbscan <- function(truck_obs, eps = 0.0001, minPts = 2) {
  
  # stop points
  stop_points <- 
    truck_obs[speed_status == "Stop"]
  
  # stop point coordinates
  stop_coordinates <- 
    stop_points |> 
    st_as_sf() |>
    st_coordinates()
  
  # construction of groups using dbscan
  stop_groups <- dbscan::dbscan(stop_coordinates, eps = eps, minPts = minPts)
  
  # data table for export
  cbind(stop_points, stop_group = stop_groups$cluster)
}

# function to identify the points within the piler neighborhood

aa_piler_neighborhood <- function(truck_obs, piler_shape) {
  
  # piler shape
  piler_shape <- auxiliary_shapes |> 
    filter(shape_type == piler_shape)
  
  aa_within(truck_obs, piler_shape)
}

# function to calculate piler time for rleid and dbscan

aa_piler_time <- function(stop_groups, stop_offset){
  
  last_stop <- max(stop_groups$stop_group)
  
  stop_groups |> 
    filter(stop_group == (last_stop - stop_offset)) |> # filter the stop group
    pull(time_diff) |> # get the time differences
    sum() / 60 # sum the time differences and divide by 60 to get minutes
}

# map function

aa_stop_map <- function(piler_group, offload_shape) {
  
  stopPal <- colorFactor(palette = "Reds", domain = piler_group$stop_group)
  
  # stop_obs <- truck_obs[speed_status == "Stop"] |> st_as_sf()
  
    leaflet() |>
    addProviderTiles(provider = providers$Esri.WorldImagery) |>
    addCircleMarkers(
      data = piler_group |> st_as_sf(),
      radius = 3,
      color = ~ stopPal(stop_group)
    ) |> 
    addPolygons(data = auxiliary_shapes |>
                  filter(shape_type == offload_shape), 
                  color = "blue",
                  fillColor = "transparent")
}

# test the functions
# calculate the stop groups

load <- load_6

rleid_groups <- aa_piler_rleid(truck_obs = load)
dbscan_groups <- aa_piler_dbscan(truck_obs = load,
                                 eps = 0.0001,
                                 minPts = 2)
# calculate the time on piler

rleid_time <- aa_piler_time(stop_groups = rleid_groups,
                            stop_offset = 0)
dbscan_time <- aa_piler_time(stop_groups = dbscan_groups,
                             stop_offset = 0)

# times on the piler

rleid_time
dbscan_time

# map of stop clusters

aa_stop_map(piler_group = rleid_groups,
            offload_shape = "offload north")
aa_stop_map(piler_group = dbscan_groups,
            offload_shape = "offload north")







