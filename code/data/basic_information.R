# AgriNW farm info
# Initial: 27 September 2023
# Revision: 15 January 2024
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  tidyverse,
  leaflet,
  DT
)

require(analyzeWork)
require(harvestManager)
source("connections.R")
source('functions/utility_functions.R')

# farm id 

farm_id <-
  aa_name_please(name = "AgriNorthwest", con = prod_mysl) |> 
  pull(entity_uuid)

start <- "2023-09-01Z12:22:24T"

end <- "2023-12-31Z12:22:24T"

# geography information --------------------------------------------------------

geography <-
  farm_sf(farm_uuid = farm_id,
          start = start,
          end = end,
          con = prod_mysl,
          es_connection = prod_elastic,
          swap_coords = FALSE) |> 
  ungroup() |> 
  filter(!str_detect(name, 'Test')) |> 
  mutate(geo_region_type = factor(geo_region_type_id,
                           levels = c(2, 4, 9),
                           labels = c('field', 'offload', 'shop')))

# remove duplicate fields some geo_region_shape_uuids refer to same name

geography <- 
  geography |> 
  group_by(name) |> 
  arrange(name, start_date) |> 
  slice_tail() |> 
  ungroup()

# for labels calculate geo centers

geo_area <- 
  aa_acreage(geography$geometry)

yield_per_acre <- 
  runif(nrow(geography), 450, 650) |> round(1)

geography <- 
  cbind(geography, geo_area = geo_area, yield_per_acre = yield_per_acre) |> 
  mutate(variety = 'Potato',
         total_yield = (geo_area * yield_per_acre) |> round(0)) |> 
  select(name, geo_region_uuid, geo_region_type, variety,
         total_yield, geo_area, yield_per_acre)

# shapes for farm, offload, and shop

farm_bbox <- 
  geography |> 
  st_bbox()|> 
  st_as_sfc() |> 
  st_as_sf()

offload_shape <- 
  geography |> 
  filter(geo_region_type == 'offload') |> 
  st_transform(crs = 'EPSG:3587') |> 
  st_union() |> 
  st_minimum_rotated_rectangle() |> 
  st_transform(crs = 'EPSG:4326') |> 
  st_as_sf()

shop_shape <- 
  geography |> 
  filter(geo_region_type == 'shop') |> 
  st_geometry() |> 
  st_as_sf()

crop_fields <- 
  geography |> 
  filter(geo_region_type == 'field') |> 
  select(name, variety, total_yield, geo_area, yield_per_acre)

# Farm Map

aa_farm_map <- function(farm_bbox, crop_fields, offload_shape, shop_shape) {
  farm_bbox |>
    leaflet() |> 
    addProviderTiles(provider = providers$Esri.WorldImagery) |>
    addPolygons(
      color = 'transparent',
      weight = 0.5
    ) |> 
    addPolygons(data = crop_fields,
                color = 'green',
                fillOpacity = 0.3,
                weight = 2,
                popup =  ~ (paste("Field:", name, "<br>",
                                  "Total Yield:", total_yield, "sacks", "<br>", 
                                  "Acreage:", geo_area, "acres", "<br>",
                                  "Yield Per Acre: ", yield_per_acre, "sacks")),
                group = "Fields") |> 
    addPolygons(data = offload_shape,
                color = 'yellow',
                fillOpacity = 0.5,
                weight = 2,
                group = "Offload") |>
    addPolygons(data = shop_shape,
                color = "red",
                fillOpacity = 0.5,
                weight = 2,
                group = "Shop") |> 
    addLayersControl(
      overlayGroups = c('Fields', "Offload", "Shop"),
      options = layersControlOptions(collapsed = FALSE,
                                     position = "bottomright")
    )
}

aa_farm_map(
  farm_bbox = farm_bbox,
  crop_fields = crop_fields,
  offload_shape = offload_shape,
  shop_shape = shop_shape
)

# Table of fields

crop_fields |> 
  st_drop_geometry() |> 
  datatable()

# Map of fields

aa_shape_map <- function(shape, outline_color){
  shape |>
    leaflet() |> 
    addProviderTiles(provider = providers$Esri.WorldImagery) |>
    addPolygons(
      color = outline_color
    )
}

harvest_field_names <- c("2718", "2734")

harvest_fields <- 
  crop_fields |> 
  filter(name %in% harvest_field_names)

aa_shape_map(shape = offload_shape,
             outline_color = 'yellow')

aa_shape_map(shape = harvest_fields,
             outline_color = 'green')
  


# import harvest days information

harvest_days <-
  readxl::read_excel("F:/AgriNW/Farm Information.xlsx", 
                     sheet = "harvest days")

# Device ids

device_ids <-
  aa_farm_devices(
    farm_uuid = farm_id,
    start = start,
    end = end,
    con = prod_mysl
  ) |> 
  pull(device_uuid)

# Device information

source("connections.R") # reload the connections file if data base error

device_info <-
  device_details(device_uuid = device_ids,
                 start = start,
                 end = end,
                 con = prod_mysl) |> 
  ungroup() |> 
  select(name, device_uuid, equipment_type_id)

missing_tractor <- tibble(
  name = 'M',
  device_uuid = 'ef181a54-d3ea-45d9-b85f-80662eee87b5',
  equipment_type_id = 1
)

device_info <- 
  rbind(device_info, missing_tractor)

trucks <-
  device_info |> 
  filter(equipment_type_id == 3) |> 
  select(name, device_uuid) |> 
  distinct(.keep_all = TRUE)

tractors <-
  device_info |> 
  filter(equipment_type_id == 1) |> 
  select(name, device_uuid) |> 
  mutate(harvest_rows = 6,
         row_width = 34)

# Device ids

truck_ids <-  
  trucks |> 
  distinct(device_uuid) |> 
  pull(device_uuid)

tractor_ids <- 
  tractors |> 
  distinct(device_uuid) |> 
  pull(device_uuid)


# import harvest days information

harvest_days <-
  readxl::read_excel("F:/AgriNW/Farm Information.xlsx", 
                     sheet = "harvest days")

# save to AgriNW workspace

objects_list <- c('geography', 'trucks', 'tractors', 'truck_ids',
                  'tractor_ids', 'harvest_days', 'offload_shape',
                  'shop_shape', 'potato_bbox')

save(list = objects_list, file = "F:/AgriNW/AgriNW_info.RData")
