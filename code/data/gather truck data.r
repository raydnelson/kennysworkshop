# Get the truck data for AgriNW Farms
# Initial: 23 October 2023
# Revision: 30 October 2023
# Data Team: Jon and Ray

# Load Libraries ---------------------------------------------------------------
pacman::p_load(
  sf,
  tidyverse
)

# load -------------------------------------------------------------------------

load("F:/AgriNW/AgriNW_info.RData")

# Time parameters

require(analyzeWork)
require(harvestManager)
source("connections.R")

# Gather all of the truck data
    
start <- '2023-09-08Z00:00:00T'
end <- '2023-10-14Z23:59:59T'
    
# truck gps --------------------------------------------------------------------
    
truck_gps <-
  truck_ids |>
  map(
    .f = function(x) {
      eg_data_extractor(
        device_uuid = x,
        start = start,
        end = end,
        elastic_connection = prod_elastic
      )
    }
  )

truck_gps <- truck_gps[sapply(truck_gps, is.data.frame)]

truck_gps <-
  map_df(
    .x = 1:length(truck_gps),
    .f = function(x) {
      truck_gps[[x]]
    }
  )

truck_gps <-
  truck_gps |>
  st_as_sf(crs = 4326,
           coords = c("loc_point1", "loc_point2"))

# file name

filename <- 'F:/AgriNW/raw/trucks.RData'
    
# save to workspace

save(list = c("truck_gps"), file = filename)


