# Prepare data for harvest 2023
# Initial: 29 March 2024
# Revision: 29 March 2024
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  leaflet,
  data.table,
  clock,
  arcgisbinding,
  DT,
  gt,
  tidyverse
)

# load data

load("F:/Acuitus/AgriNW/harvest_2023.RData")

# remove auxiliary data

rm(auxiliary_areas, auxiliary_shapes)

# get arcgis functions

source("../aa_libraries/arcgis functions.R")

# get auxiliary shapes from arcgis

auxiliary_shapes <- 
  aa_arc_open(farm = "AgriNW",
            feature_class = "auxiliary_areas")

# cleanup functions

rm(aa_arc_open, aa_arc_write)

# clean up crops and fields

crop_fields <- 
  crop_fields |> 
  select(name)

# clean up shapes

rm(motor_pool_shape, offload_shape, shop_shape)

# save workspace for harvest 2023

save(list = ls(), file = "F:/Acuitus/AgriNW/harvest_2023_clean.RData")
