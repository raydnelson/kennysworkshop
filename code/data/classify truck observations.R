# Classify truck observations
# Initial: 1 April 2024
# Revision: 2 Aprill 2024
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  leaflet,
  data.table,
  clock,
  arcgisbinding,
  DT,
  gt,
  tidyverse
)

# load data

load("F:/Acuitus/AgriNW/harvest_2023.RData")

# function to find observations within

aa_within <- 
  function(feature, shape) {
    st_within(feature, shape, sparse = FALSE) |> as.logical()
  }

# shapes for shop area and shop

shop <- auxiliary_shapes[shape_type == "shop area"] |> st_as_sf()
offload <- auxiliary_shapes[shape_type == "offload area"] |> st_as_sf()
motor_pool <- auxiliary_shapes[shape_type == "motor pool"] |> st_as_sf()

# create variables for shop, offload, and motor pool

truck_gps<- 
  truck_gps |> 
  mutate(
    shop = aa_within(geometry, shop),
    offload = aa_within(geometry, offload),
    motor_pool = aa_within(geometry, motor_pool)
  )

# combine shop, offload, and motor pool into status variable

truck_gps <-
  truck_gps[, "status" :=
              case_when(offload ~ "Offload",
                        motor_pool ~ "Motor Pool",
                        shop ~ "Shop",
                        TRUE ~ "Transit/Field")] |>
  mutate(status = factor(
    status,
    levels = c("Motor Pool", "Transit/Field", "Offload", "Shop")
  )) |> 
  select(-offload, -motor_pool, -shop)

# save shapes to RData file

shape_list <- c("auxiliary_shapes", "crop_fields", "motor_pool",
                "offload", "shop")

save(list = shape_list, file = "F:/Acuitus/AgriNW/shapes.RData")

# cleanup

rm(list = shape_list)
rm(aa_arc_write)
rm(aa_within, shape_list, trucks_in_shop)

# save workspace as harvest.RData

save.image("F:/Acuitus/AgriNW/harvest.RData")
