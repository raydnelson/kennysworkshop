# Scratch fields, tractors, and trucks
# Initial: 2 October 2023
# Revision: 2 October 2023
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  leaflet,
  tidyverse
)

# load data

# load('F:/AgriNW/tractors_trucks.RData')
load('F:/AgriNW/AgriNW_info.RData')
# source('functions/utility_functions.R')

# Map of Fields

aa_geo_regions_map <- function(geography) {
  geo_region_pal <- colorFactor('Set1', geography$geo_region_type)
  
  field_map <- 
    geography |>
    leaflet() |> 
    addProviderTiles(provider = providers$Esri.WorldImagery)
  
  geo_types <- levels(geography$geo_region_type)
  
  geo_types |>
    walk(
      .f = function(x) {
        field_map <<-
          field_map |>
          addPolygons(
            data = geography |> filter(geo_region_type == x),
            fillColor = ~ geo_region_pal(geo_region_type),
            color = ~ geo_region_pal(geo_region_type),
            weight = 1,
            fillOpacity = 0.25,
            group = x
          ) |>
          addCircleMarkers(
            data = geography |>
              filter(geo_region_type == x) |>
              st_set_geometry('geo_center'),
            radius  = 10,
            color = 'transparent',
            label = ~ htmltools::htmlEscape(name)
          )
      }
    )
  
  field_map |>
    addLayersControl(
      overlayGroups = geo_types,
      options = layersControlOptions(collapsed = FALSE)
    )
}

# Table Summary

aa_fields_table <- function(geography) {
  geography |> 
    filter(geo_region_type == 'field') |> 
    select(name, geo_area, variety) |> 
    st_drop_geometry() |> 
    DT::datatable(rownames = FALSE,
                  colnames = c('Name', 'Area', 'Potato Variety'))
}

geography |> aa_geo_regions_map()
geography |> aa_fields_table()



