# Trucks in shop
# Initial: 16 October 2023
# Revision: 18 October 2023
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  leaflet,
  tidyverse,
  gt,
  DT
)

# load data
load('F:/AgriNW/agriNW_info.RData')
load('F:/AgriNW/tractors_trucks.RData')

# observations for truck in shop

trucks_in_shop <-
  truck_gps |> 
  mutate(month_day = date(ts),
         shop = st_within(geometry, shop_shape, sparse = FALSE) |>
           as.logical()) |> 
  filter(shop == TRUE)

truck_summary <- 
  trucks_in_shop |> 
  st_drop_geometry() |> 
  group_by(month_day, device_id) |> 
  summarize(first = min(ts),
            last = max(ts)) |> 
  mutate(time_in_shop = difftime(last, first, units = 'mins') |> 
           round(0)) |> 
  mutate(first = format(first, '%H:%M'),
         last = format(last, '%H:%M'),
         month_day = format(month_day, '%m-%d'))

truck_summary

truck_summary |> 
  gt()

truck_summary |> 
  datatable()

save('truck_summary', file = 'F:/AgriNW/harvest/truck_summary.RData')
  
# test truck
  
  test_truck <- 
  truck_gps |> 
  filter(device_id == truck_ids[1])

big_shop <- 
  shop_shape |> 
  st_buffer(dist = 10)

big_shop |>
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |>
  addPolygons(
    color = 'red'
  )
  
  

  # Within the shop
  
  test_truck_in_shop <- 
  test_truck |> 
  mutate(shop = st_within(geometry, shop_shape, sparse = FALSE)) |> 
  filter(shop == TRUE & lag(shop) == FALSE)


test_truck_13 <- 
  test_truck |> 
  filter(day(ts) == 13) |> 
  mutate(shop = st_within(geometry, big_shop, sparse = FALSE) |>
           as.logical())

# Observations for all trucks in the harvest interval

all_trucks <- 
  truck_gps |> 
  filter(ts %within% harvest_interval)

all_trucks |>
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |>
  addPolygons(
    data = field_shape,
    color = 'red'
  ) |> 
  addPolygons(
    data = offload_shape,
    color = 'yellow'
  ) |> 
  addPolygons(
    data = shop_shape,
    color = 'green'
  ) |> 
  addCircleMarkers(
    color = 'blue',
    radius = 1
  )

trucks_in_shop <- 
  all_trucks[shop_shape, ]

trucks_in_shop |> 
  st_drop_geometry() |> 
  count(device_id)

# Trucks in the shop shape

trucks_in_shop <- 
  truck_gps[shop_shape,]
  
# harvest days
  
harvest_days <- 
  tractor_gps |> 
  mutate(harvest_day = ts |> format('%m-%d') |> as.factor())

harvest_days |> 
  st_drop_geometry() |> 
  group_by(harvest_day) |> 
  summarize(start = min(ts) |> format('%H:%M'),
            finish = max(ts) |> format('%H:%M'))

harvest_days |> 
  filter(harvest_day == '09-08')

# Choose the trucks

chosen_day <- ymd('2023-09-08')

chosen_day + ddays(1)

harvest_days$ts <= chosen_day

day(chosen_day)

month <- 9
day <- 8

aa_chosen_day <- function(sf_object, month, day){
  sf_object |> 
    filter(day(ts) == day,
           month(ts) == month)
}

first_day <-
  aa_chosen_day(sf_object = tractor_gps,
                month = 9,
                day = 8)

tractor_gps |>
  slice_head(n = 4500) |> 
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |>
  addCircleMarkers(
    radius = 1,
    color = 'yellow'
  )

all_day <- 
  aa_chosen_day(
    sf_object = tractor_gps,
    month = 9,
    day = 21
  )

first_day |> 
  slice(4208:4324) |> 
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |>
  addCircleMarkers(
    radius = 1,
    color = 'yellow'
  )

first_day |> 
  filter(!(row_number() %in% c(4208:4324))) |> 
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |>
  addCircleMarkers(
    radius = 1,
    color = 'yellow'
  )

truck_gps |> 
  aa_chosen_day(
    month = 9,
    day = 18
  ) |> 
  slice(50000:60000) |> 
    leaflet() |> 
    addProviderTiles(provider = providers$Esri.WorldImagery) |>
    addCircleMarkers(
      color = 'red',
      radius = 1
    )

field_shape |>
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |>
  addPolygons(
    color = 'red'
  ) |> 
  addPolygons(
    data = offload_shape,
    color = 'yellow'
  ) |> 
  addPolygons(
    data = shop_shape,
    color = 'blue'
  )

field_summary[[160]]$current_truck_shop |> 
    leaflet() |> 
    addProviderTiles(provider = providers$Esri.WorldImagery) |>
    addPolygons(data = shop_shape,
      color = 'red'
    ) |> 
  addCircleMarkers(
    color = 'yellow',
    radius = 5
  )

aa_geo_map(
  current_truck =
    field_summary[[100]]$current_truck_shop,
  geo_shape = shop_shape
)
    
  


  
 
  
