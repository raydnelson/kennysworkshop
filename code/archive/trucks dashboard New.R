# Trucks in field
# Initial: 4 October 2023
# Revision: 5 October 2023
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  leaflet,
  gt,
  tidyverse
)

# load data

load('F:/AgriNW/fields/field_2382.RData')
load('F:/AgriNW/harvest_progress.RData')


# when are trucks in field

within_field <-
  st_within(truck_gps$geometry,
            field_shape,
            sparse = FALSE) |>
  as.vector()

truck_gps <- 
  cbind(truck_gps, within_field) |> 
  mutate(enter = within_field == TRUE & lag(within_field) == FALSE,
         exit = within_field == FALSE & lag(within_field) == TRUE)

# enter and exit

enter_exit <- 
  truck_gps |> 
  filter(enter == TRUE | exit == TRUE) |> 
  arrange(device_id, ts)

# arbitrary time

harvest_hour <- 17

given_time <- harvest_interval |> int_start() + 3600 * harvest_hour

# time in field calculations

time_in_field <-
  enter_exit |>
  filter(ts <= given_time) |>
  group_by(device_id) |>
  slice_max(ts) |>
  filter(exit == FALSE) |>
  arrange(ts) |>
  mutate(time_in_field = difftime(given_time,
                                  ts,
                                  units = 'mins') |> 
           round(1)) |>
  select(device_uuid = device_id, entry_time = ts, time_in_field) |>
  ungroup()

# Current truck positions

current_truck <- 
  truck_gps |> 
  filter(ts <= given_time) |> 
  group_by(device_id) |> 
  slice_max(ts) |> 
  filter(within_field == TRUE)

# Current tractor position

current_tractor <- 
  tractor_gps |> 
  dplyr::filter(ts <= given_time) |> 
  group_by(device_id) |> 
  slice_max(ts)

# map of tractors and trucks and harvest progress

harvest_progress[[harvest_hour]]$harvest_acreage

harvest_progress[[harvest_hour]]$harvest_polygon |>
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |>
  addPolygons(data = field_shape,
              color = 'lightgreen',
              fillColor = 'transparent') |> 
  addPolygons(
    color = 'transparent',
    fillColor = 'orange',
    fillOpacity = 0.6
  ) |> 
  addCircleMarkers(data = current_truck,
                   color = 'red',
                   radius = 2
  ) |> 
  addCircleMarkers(data = current_tractor,
                   color = 'blue',
                   radius = 2)


# table of time in field

time_in_field |> 
  st_drop_geometry() |> 
  left_join(trucks) |> 
  select(name, entry_time, time_in_field) |> 
  distinct(.keep_all = TRUE) |> 
  gt()


  
