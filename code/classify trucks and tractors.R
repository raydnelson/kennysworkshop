# Fields with tractors and trucks
# Initial: 17 April 2024
# Revision: 19 April 2024
# Data Team: Jon and Ray

# Load Libraries
pacman::p_load(
  sf,
  leaflet,
  data.table,
  clock,
  arcgisbinding,
  DT,
  gt,
  tidyverse
)

# load data

load("F:/Acuitus/AgriNW/harvest.RData", envir = .GlobalEnv)
load("F:/Acuitus/AgriNW/shapes.RData", envir = .GlobalEnv)

# Identify trucks and tractors in each field. Create a list

tractors_trucks <-
  crop_fields$name |> 
  map(
    .f = function(x) {
      
    field_shape <- 
      crop_fields |> 
      filter(name == x)
    
    tractor_obs <- 
      tractor_gps[st_within(geometry, field_shape, sparse = FALSE) |> 
                    as.logical()]
    
    truck_obs <- 
      truck_gps[st_within(geometry, field_shape, sparse = FALSE) |> 
                  as.logical()]
    
    list(tractor_obs = tractor_obs, truck_obs = truck_obs)
    }
  )

names(tractors_trucks) <- crop_fields$name

save(list = c("tractors_trucks"),
     file = "F:/Acuitus/AgriNW/tractors_trucks.RData")

# trucks in field

tractor_gps <-
  tractor_gps |> 
  mutate(status = "Transit")

# assign each tractor observation to its

crop_fields$name |> 
  walk(
    .f = function(x) {
      
      field_shape <- 
        crop_fields |> 
        filter(name == x)
      
      field_obs <- 
        st_within(tractor_gps$geometry, field_shape, sparse = FALSE) |> 
                      as.logical()
      
      tractor_gps[field_obs, status := x]
    }
  )

# save tractor gps

save(list = c("tractor_gps"),
     file = "F:/Acuitus/AgriNW/tractor_gps.RData")

# find harvested fields from among the 65 possible fields

harvested_field_names <- 
  tractor_gps |> 
  count(status) |> 
  filter(n > 500) |> 
  filter(status != "Transit") |> 
  pull(status)

# harvested field names from harvested_fields

harvested_fields |> 
  count(field_name)

# add status to truck_gps

truck_gps <-
  truck_gps |> 
  mutate(status = "Transit")

# assign each truck observation to its field

harvested_field_names |> 
  walk(
    .f = function(x) {
      
      field_shape <- 
        crop_fields |> 
        filter(name == x)
      
      field_obs <- 
        st_within(truck_gps$geometry, field_shape, sparse = FALSE) |> 
                      as.logical()
      
      truck_gps[field_obs, status := x]
    }
  )

# truck gps obs in shop

shop_obs  <- 
  st_within(truck_gps$geometry, shop, sparse = FALSE) |> 
  as.logical()

truck_gps[shop_obs, status := "Shop"]

# truck gps obs at offload

offload_obs  <- 
  st_within(truck_gps$geometry, offload, sparse = FALSE) |> 
    as.logical()

truck_gps[offload_obs, status := "Offload"]

# truck gps obs at motor pool

motor_pool_obs  <- 
  st_within(truck_gps$geometry, motor_pool, sparse = FALSE) |> 
    as.logical()

truck_gps[motor_pool_obs, status := "Motor Pool"]

# count truck status

truck_gps |> 
  count(status)

# Save truck_gps

save(list = c("truck_gps"),
     file = "F:/Acuitus/AgriNW/truck_gps.RData")

# load trucks, tractors, and shapes

load("F:/Acuitus/AgriNW/shapes.RData", envir = .GlobalEnv)
load("F:/Acuitus/AgriNW/truck_gps.RData", envir = .GlobalEnv)
load("F:/Acuitus/AgriNW/tractor_gps.RData", envir = .GlobalEnv)

# bounding box of the farm defined for crop_fields

farm_bbox <- 
  st_bbox(crop_fields) |> 
  st_as_sfc() |> 
  st_buffer(dist = 500) |> 
  st_as_sf()

# map of farm bounding box

farm_bbox |> 
  leaflet() |> 
  addProviderTiles(provider = providers$Esri.WorldImagery) |> 
  addPolygons() |> 
  addPolygons(data = crop_fields,
              color = 'blue',
              fillColor  = 'green',
              fillOpacity = 0.3,
              weight = 2)

# exclude motor pool from trucks gps

truck_gps <- 
  truck_gps |> 
  filter(status != "Motor Pool")

# truck observations that are within the farm bounding box

truck_gps <- 
  truck_gps[st_within(geometry, farm_bbox, sparse = FALSE) |> 
              as.logical()]

# exclude transit from tractor gps

tractor_gps <- 
  tractor_gps |> 
  filter(status != "Transit")

# tractor observations that are within the farm bounding box

tractor_gps <- 
  tractor_gps[st_within(geometry, farm_bbox, sparse = FALSE) |> 
                as.logical()]

# save tractor_gps and truck_gps to devices.RData

save(list = c("tractor_gps", "truck_gps"),
     file = "F:/Acuitus/AgriNW/devices.RData")

# remove tractpr_gps and truck_gps and save workspace

rm(tractor_gps, truck_gps)

save.image("F:/Acuitus/AgriNW/shapes.RData")

# load devices.RData

load("F:/Acuitus/AgriNW/devices.RData", envir = .GlobalEnv)

# load shapes

load("F:/Acuitus/AgriNW/shapes.RData", envir = .GlobalEnv)

# remove tractor_gps and truck_gps

rm(tractor_gps, truck_gps)

# save image

save.image("F:/Acuitus/AgriNW/harvest.RData")
