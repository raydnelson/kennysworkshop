# geography functions
# Initial: 8 April 2024
# Revision: 24 April 2024
# Data Team: Jon and Ray

# function to create a datatable of fields

aa_fields_DT <- function(crop_fields) {
  crop_fields |> 
    st_drop_geometry() |>
    select(name, crop, area, start_time, end_time) |>
    datatable(rownames = FALSE,
              options = list(pageLength = 20, scrollX = TRUE),
              colnames = c("Field Name", "Crop", "Area (Acres)",
                           "Start Time", "End Time")) |> 
    formatRound(columns = "area", digits = 1) |> 
    formatDate(c("start_time","end_time"), "toLocaleString")
}

# harvester_summary_DT

aa_harvester_summary_DT <- function(harvester_summary) {
  harvester_summary |> 
    select(device_id, nobs, distance) |>
    datatable(rownames = FALSE,
              colnames = c("Device ID",
                           "Number of Observations",
                           "Distance (miles)"),
              selection = list(
                target = 'row',
                selected = 1)) |> 
    formatRound(columns = "distance", digits = 1)
}

# data table for trucks and tractors

aa_truck_summary_DT <- function(truck_summary) {
  truck_summary |>
    datatable(rownames = FALSE,
              options = list(pageLength = 15, scrollX = TRUE),
              colnames = c("Device", "Obs"),
              selection = list(
                mode = 'single',
                target = 'row',
                selected = 1)
              )
}

# data table for truck observations

aa_obs_DT <- function(chosen_obs) {
  chosen_obs |>
    select(local_time, speed) |>
    datatable(
      rownames = FALSE,
      options = list(pageLength = 15, scrollX = TRUE),
      colnames = c("Time", "Speed")
    ) |>
    formatRound("speed", 1) |> 
    formatDate("local_time", "toLocaleTimeString")
}

# offload summary datatable

aa_offload_summary_DT <- function(offload_summary){
  offload_summary |> 
    datatable(rownames = FALSE,
              options = list(pageLength = 15, scrollX = TRUE),
              colnames = c("Device", "Total Minutes", "Obs"),
              selection = list(
                mode = 'single',
                target = 'row',
                selected = 1))
}

# load summary datatable

aa_load_summary_DT <- function(offload_summary){
  offload_summary |> 
    datatable(rownames = FALSE,
              options = list(pageLength = 15, scrollX = TRUE),
              colnames = c("Load", "Start", "End", "Offload", "Obs"),
              selection = list(
                mode = 'single',
                target = 'row',
                selected = 1)) |> 
    formatDate(c("start", "end"), "toLocaleTimeString")
}

# load observations

aa_load_obs_DT <- function(load_obs){
  load_obs |>
    select(local_time, time_diff, speed, speed_status) |>
    datatable(rownames = FALSE,
              options = list(pageLength = 15,
                             scrollX = TRUE),
              colnames = c("Time", "Time Diff", "Speed", "Speed Status")) |>
    formatRound("speed", 1) |> 
    formatDate("local_time", "toLocaleTimeString")
}

# shop summary datatable

aa_shop_summary_DT <- function(shop_summary){
  shop_summary |> 
    datatable(rownames = FALSE,
              options = list(pageLength = 15, scrollX = TRUE),
              colnames = c("Device", "Start", "End", "Total Minutes", "Obs"),
              selection = list(
                mode = 'single',
                target = 'row',
                selected = 1))
}