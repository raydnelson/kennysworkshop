# Daily Offload
# Initial: 9 April 2024
# Revision: 13 May 2024
# Data Team: Jon and Ray

# Harvest ---------------------------------------------------------------------

tab_daily_offload <- tabItem(
  tabName = "daily_offload",
  h2("Offload"),
  
  fluidRow(
    
    aa_DT_box(outputID = "offload_summary",
              title = "Device Summary",
              width = 4,
              box_height = 900),

    aa_DT_box(outputID = "load_summary",
              title = "Loads",
              width = 4,
              box_height = 900),
    
    box(
      title = "Map",
      status = "primary",
      solidHeader = TRUE,
      width = 4,
      height = 900,
      
      tabBox(
        
        width = 12,
        tabPanel("Daily Map",
                 leafletOutput(outputId = "offload_map",
                               height = 700)),
        tabPanel("Daily Data",
                 DT::DTOutput(outputId = "offload_table",
                              height = 700)),
        tabPanel("Load Map",
                 leafletOutput(outputId = "load_map",
                               height = 700)),
        tabPanel("Load Data",
                 DT::DTOutput(outputId = "load_table",
                              height = 700),
                 fluidRow(
                   box(width = 6,
                       title = "Create Load Object",
                       status = "success",
                       actionButton(
                         inputId = "save_load_btn",
                         label = "Save Load",
                         style="color: #fff;
                    background-color:  #2E7D32;
                    border-color: #2e6da4")),
                   
                   box(width = 6,
                       title = "Object Name",
                       status = "success",
                       textInput("object_name",
                                 label = NULL,
                                 value = "load_"))
                   )
                 )
        )
      )
    )
   )

